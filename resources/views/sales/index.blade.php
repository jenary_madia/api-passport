@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Sales</h1>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12">
          <div class="box">
            <div class="box-body">
              <table class="table sales-table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th>Progress</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.box -->
    </div>
  </div>
@stop

@section('adminlte_js')
  <script>
    $(document).ready(function() {
      $('.sales-table').DataTable(
        {
          serverSide: true,
          processing: true,
          responsive: true,
          ajax: "{{ route('sales.datatable') }}",
          order : [[ 0, "desc" ]],
          columns: [
              { data: 'down_payment' },
              { data: 'down_payment' },
              { data: 'n_model' },
          ],
        }
      );
    })
  </script>
@endsection