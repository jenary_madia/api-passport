@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Stores</h1>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12">
          <div class="box">
            <div class="box-body">
              <table class="table sales-table table-bordered table-responsive">
                <thead>
                  <tr>
                    <th style="width: 10px">Store Name</th>
                    <th>Dealer Name</th>
                    <th>Mall</th>
                    <th>Location</th>
                    <th>Date uploaded</th>
                    <th>Uploaded By</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.box -->
    </div>
  </div>
@stop

@section('adminlte_js')
  <script>
    $(document).ready(function() {
      $('.sales-table').DataTable(
        {
          serverSide: true,
          processing: true,
          responsive: true,
          ajax: "{{ route('stores.datatable') }}",
          order : [[ 0, "desc" ]],
          columns: [
              { data: 'name' },
              { data: 'dealer_name' },
              { data: 'mall_name' },
              { data: 'branch_address' },
              { data: 'created_at' },
              { data: 'creator_name' },
              { data: 'id',render: function(data) {
                  return `<button class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></button> <button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>`
                } 
              },
          ],
        }
      );
    })
  </script>
@endsection