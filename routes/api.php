<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/login', 'Api\AccessTokenController@issueToken');

Route::middleware(['auth:api'])->group(function () {
		Route::get('/validate-token', function () {
    		// return {true};
		});
		Route::post('/register','RegisterController@register');
    	Route::prefix('store')->group(function () {
	        Route::post('/add', 'Api\StoreController@add');
	        Route::post('/delete', 'Api\StoreController@delete');
	        Route::post('/list2/{max_id?}', 'Api\StoreController@list2');
	        Route::post('/{id}', 'Api\StoreController@update');
	        Route::get('/list', 'Api\StoreController@list');
	        Route::get('/{id}', 'Api\StoreController@get');
	    });
	    // User
	    Route::prefix('user')->group(function () {
	        Route::get('/list', 'UserController@list');
	        Route::post('/delete', 'UserController@deactivateUser');
	    });
	    // TEST
	    Route::prefix('test')->group(function () {
	        Route::post('/add', 'Api\StoreController@addTest');
	    });

	    Route::prefix('sales')->group(function () {
	    	Route::get('/dropdowns', 'Api\SalesController@fetchSelections');
	    	Route::post('/store', 'Api\SalesController@store');
	    	Route::post('/list', 'Api\SalesController@list');
	    	Route::post('/search', 'Api\SalesController@search');
	    });

	    Route::post('logout','UserController@logoutApi');
});

