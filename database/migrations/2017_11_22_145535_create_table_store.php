<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id')->nullable();
            $table->string('name')->nullable();
            $table->string('promoter')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('dealer_name')->nullable();
            $table->string('mall_name')->nullable();
            $table->string('region')->nullable();
            $table->string('image')->nullable();
            $table->string('branch_address')->nullable();
            $table->string('branch_contact_no')->nullable();
            $table->string('branch_email')->nullable();
            $table->decimal('weekly_sales_amount',10,2)->nullable();
            $table->integer('weekly_sales_qty')->nullable();
            $table->string('existing_vd')->nullable();
            $table->string('recent_dut')->nullable();
            $table->string('recent_dum')->nullable();
            $table->string('product_mix')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
