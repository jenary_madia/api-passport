<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_details', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('creator_id')->nullable();
            $table->integer('store_id')->nullable();
            $table->string('dealer_menu')->nullable();
            $table->string('region')->nullable();
            $table->string('model')->nullable();
            $table->string('color')->nullable();
            $table->integer('qty')->nullable();
            $table->decimal('down_payment',10,2)->nullable();
            $table->integer('week_no');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_details');
    }
}
