<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'Super Admin',
            'email' => 'admin2314@gmail.com',
            'password' => bcrypt('admin2314'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);   //
    }
}
