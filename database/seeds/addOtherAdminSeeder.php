<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class addOtherAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
     	DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'Super Admin 2',
            'email' => 'tad',
            'password' => bcrypt('mywidgets081881'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);   //
    }
}
