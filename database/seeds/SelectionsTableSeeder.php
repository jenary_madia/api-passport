<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SelectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('dealers')->delete();
    	DB::table('dealers')->insert([
			[
				"name" => "3G TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "7BOX UP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "8 MOBILE INC DUMAGUETE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "8 MOBILE INC PALAWAN",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "8 TELECOM DAVAO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "8MGM TRADING INC.",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "8TC ENTERPRISES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "A & A MERCHANDISING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACCENT HUB",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACCENT HUB BAGUIO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACCENT HUB FESTIVAL",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACCENT HUB ORTIGAS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACCENT MICRO PRODUCTS, I",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACCENT PLUS INC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACCESS ONE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACJ SAN LAZARO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ACX CELLSHOPPE STORE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ADI TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "AEROPHONE CEBU",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "AEROPHONE MANILA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "AFT TOY WORLD",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ALDANA CELLPHONES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ALJON/TOJAN CELLSHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "AMB COMMUNICATIONS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ANGTO HO BUSINESS CENTER",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "APOLLO COMMERCIAL TRADING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ARNET SELLPHONE AND ACCESSO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BANANA (PALAWAN) CASHEW GO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BANANA TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BANANA TELECOM CEBU",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BCG",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BENERSON ENTERPRISES / CONNEQS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BERLEIN ALIMALL",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BERLEIN APALIT",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BERLEIN FAIRVIEW",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BERLEIN MOA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BERLEIN NORTH EDSA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BERLEIN PAMPANGA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BHZ CELLPHONE AND ACCESSORI",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BINGKEE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BLUE BOX ENTERPRISES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BLUELITE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BLUELITE BICUTAN",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BLUELITE MALL OF ASIA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BLUELITE MOA CONCEPT STR",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BLUELITE SM FAIRVIEW CON",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BNR CELLPHONE AND ACCESSOR",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "BOMA TECH",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CALL PLUS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CAPARAL APPLIANCES & FURNIT",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CELLCOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CELLUCOM DEVICE CENTER",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CELLWORKS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CELLWORLD",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CELLZONE COMMUNICATION",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CELROS CELLPHONE AND ACCESS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CHAIVER TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CHESTER HERRERA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CHRONOS CELLPHONE SHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CITY TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CITYLIGHT",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "COMWORKS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CONCEPT COMPUTER CENTER",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "COSMIC TECHNOLOGIES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "COUNTRY SHOPPE CELLPHONE & ACC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CY7 CELLPHONE AND ACCESSORI",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CYBERSTAR",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CYBERTAB SM CALAMBA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CYBERTAB SM FAIRVIEW",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CYBERTAB SM NORTH",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CYBERTAB SM STA ROSA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CYBERWIDGET SM MOA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "CYBERWIDGET SM ROSARIO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "D SUPER 88/TELEGO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "D' CELL CITY",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "DAET CELLCITY AND GENERA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "DG PHONE/GARBES TRADING CORP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "DIGITAL MOBILE ENGINE GR",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "DIGITAL XY CELLPHONE SHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "E PHONE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "EASY ACCESS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "EC PANDA CITY TECHNOLOGIES INC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "EMBASSY OF CHINA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ESQUIRETECH CORP.",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "FLASHER NEXT GENERATION CORP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "FLEXIFONE/REXTICO BOROMEO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "FONE STYLE ACCESSORIES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "FONERANGE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "G2 TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GADGETS & GEAR PH",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GALLEON ENTERPRISES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GC APPLIANCE CENTRUM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GIAS CELLPHONE AND ACCESSOR",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GIGAHERTZ COMPUTER SYSTEM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GR8 FASHION VENTURES (FASH",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GREEN COUNTY MERCHANDISING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GREENTELE COM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "GUANZON MDSG",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "HAITEKU DIGITAL VILLAGE,INC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "HEXACOM ENTERPRISES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "HIGHLAND CELLPHONE SHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "HONG BE TRADING (TABLET CITY)",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "HONG WO TECH INC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "HUAWEI TECHNOLOGIES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "I GIZMO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "INFINITE PHONE ACCESS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "INFONEC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "INFONEC ALABANG",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "INFONEC STA LUCIA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "INNOVASIA MARKETING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ISLAND MOBILE TRADING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "J9NTEL MARKETING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JAMES INNOVATION",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JANESKIE CELLPHONE AND ACCE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JARS CELLULAR PHONES AND AC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JEJ CELLMANIA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JEYBII FONEZONE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JMB ALBAY GADGETS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JS MOBILE CARE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JSL CELLPHONE ACCESSORIES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JUN PONCE BACOLOD",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JUNLIZ CELLPHONE AND ACCES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "JV SHOP AND SHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "K EIGHT TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "KEVIN JEFF CELLPHONE ACC AND G",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "KOKS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "KYLA CELLPHONE AND ACCESSO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "LAJ MOBILE HUB",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "LAWIN LIMA INC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "LEINHELS CELLPHONE AND ACCE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "LEINHELS CELLPHONE AND ACCESSORIES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "LIWAYWAY MARKETING CORP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "LOD TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MANIEGO'S GENERAL MERCHAND",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MANILA BULLETIN",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MCFASOLVER TECH CENTRALE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MCNARDS ENTERPRISES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MEC NETWORKS CORP.",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MEETROVI MOBILE SHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MEGACELL ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MEGAPIXEL MARKETING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MEGAWELL VENTURES AND RESO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MEMO EXPRESS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "METRO DAET CELLCITY AND GENERA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MIKE DEPT. STORE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MIKROCELL",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MILLINGTON TELECOM CENTER",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MOBILE ELEMENTS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MOBILE MATTERS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MOBILE NETWORK",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MOBILE WORKS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MOUNTAIN STUDIO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "NAVILLE LIM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "NEW INNOVATION",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "OGILVY AND MATHER PHILS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "OJIE CELLFONE SHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "OMG GADGETS & GIZMO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ONE-ONE EIGHT MARKETING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ORANGECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ORO GRAPHICS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PC CARTEL",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PERFECTCOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PICK AND CARRY SALES DIST",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PLAY TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "POWER PREMIUM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "POWER PREMIUM GROUP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "POWER PREMIUM GROUP LCTM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "POWER TAB MOA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PRESNET",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "Q SOLUTIONS COMPUTER SUPPL",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "QUBELINK",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "RC GOLDLINE/ CHRISTIAN P",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "REDHEE GENTEXT TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "RULL CEBU",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "RULL MANILA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SAMPAGUITA DEPARTMENT STORE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SAN LAZARO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SAVE N' EARN",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SHOPPER'S MART / HENRY A",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SHOPPER'S MART / HENRY ANG",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SILICON VALLEY COM GROUP P",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SILICON VALLEY COM GROUP PHILS INC.",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SIRLANCE ENTERPRISES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SIXTH SENSE INTERGRATED M",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SJAV ENTERPRISES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SKY WAVES TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SKYTECH",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SOLID ALLEGIANCE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SPARK TELECOM ANTIQUE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "SPARK TELECOM KALIBO/ROXAS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "STROM MOBILE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "STROM MOBILE FAIRVIEW",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "STROM MOBILE MARIKINA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "STROM MOBILE MOA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "STROM MOBILE NORTH EDSA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "T&J CELLPHONE AND ACCESSORI",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TECHBOX",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TECHBOX GADGETS AND ACCESSO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TECHMIX COMPUTERS AND GADGE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TECHNO FAI",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TECHNO MOBILE/RICHMOND S",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TEKPONE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "THINKING TOOLS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TIONGSAN",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TOEI",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TOWN KLART VARIETY STORE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "TRB TRADING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "UNICEL COMMUNICATION",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "UNITED ASIA AUTOMOTIVE G",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "UPGRADE.COM MOBILE AND ACC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "V TECH",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "VERTEX",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "VETTORE ENTERPRISES INC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "VIEWERS SM LIPA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "VIEWERS TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "VILLMAN COMPUTERS, INC",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "VIRAC CELLCITY",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "VIRTUALGEN TRADING CORP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "W-PLUS ELECTRONIC TRADING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "WILLGAIN",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "WOW MOBILE / ALLAN CUA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "Y TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "YLK CELLPHONE ACCESSORIES",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "YO MOBILE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "YUNLIANG MARKETING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ZEALTEXT TRADING",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ZEDRICK CELLSHOP",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ZENAR TELECOM",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "ZERO PLUS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			]
    	]);
		DB::table("models")->delete();
		DB::table('models')->insert([
			[
				"name" => "B315",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "E5330Cs",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "E5336 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "E5573s ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "G510",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "G630 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "2017",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "2017",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "4C",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PRO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "M2 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "M3",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "LITE",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "10",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PRO",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "9",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "S",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "X1",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "MT8 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "6P ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "2i",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "P10",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PLUS",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "P2 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "P7 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "P8 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "T1 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "T1PLUS ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "T2",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "T3",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "Y210D",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "Y220",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "Y3 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "Y5 ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "2017",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "II ",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "II",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			],
			[
				"name" => "PRIME",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			]
		]);

		DB::table("regions")->delete();
		DB::table('regions')->insert([
			[
			    "name" => "NORTH LUZON",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "SOUTH LUZON",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "NGMA",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "SGMA",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "VISAYAS",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "MINDANAO",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			]
		]);

		DB::table("colors")->delete();
		DB::table('colors')->insert([
			[
			    "name" => "BLACK",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "CHAMPAGNE GOLD",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "GOLD",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "GREY",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "SILVER",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WHITE",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "BLUE",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "GRAY",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "GREY",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "MOCHA",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "MOCHA BROWN",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			]
		]);

		DB::table("weeks")->delete();
		DB::table('weeks')->insert([
			[
			    "name" => "WK1",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK2",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK3",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK4",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK5",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK6",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK7",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK8",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK9",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK10",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK11",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK12",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK13",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK14",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK15",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK16",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK17",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK18",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK19",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK20",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK21",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK22",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK23",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK24",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK25",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK26",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK27",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK28",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK29",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK30",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK31",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK32",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK33",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK34",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK35",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK36",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK37",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK38",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK39",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK40",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK41",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK42",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK43",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK44",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK45",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK46",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK47",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK48",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK49",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK50",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK51",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			],
			[
			    "name" => "WK52",
			    "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
			]
		]);
    }
}
