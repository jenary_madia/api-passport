<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class otherAdminseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'Super Admin',
            'email' => 'tad',
            'password' => bcrypt('estanislao081881'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);   //
    }
}
