<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = "sales";
    protected $guarded = [];

    public function creator() {
    	return $this->hasOne("App\User","id","creator_id");
    }

    public function dealer() {
    	return $this->hasOne("App\Dealers","id","dealer_menu");
    }
}
