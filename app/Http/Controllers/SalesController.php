<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Sales;
use DataTables;

class SalesController extends Controller
{
    public function index() {
    	return view('sales.index');
    }

    public function dataTable()
    {
        $sales = Sales::join('users','sales.creator_id','users.id')
                ->leftjoin('dealers','sales.dealer_menu','dealers.id')
                ->join('regions','sales.region','regions.id')
                ->leftjoin('models','sales.model','models.id')
                ->leftjoin('colors','sales.color','colors.id')
                ->leftjoin('weeks','sales.week','weeks.id')
                ->select(DB::raw('DATE_FORMAT(sales.date, "%m-%d-%Y") as sales_date'),'sales.*','users.name as creator_name','dealers.name as dealer_name','regions.name as region_name','models.name as model_name','colors.name as color_name','weeks.name as week_name')
                ->orderBy('sales.id', 'desc');
        return DataTables::of($sales)
            ->addColumn('n_model',function($sales){
                return $sales->model_name == null ? $sales->model : $sales->model_name;
            })
            ->addColumn('n_color',function($sales){
                return $sales->color_name == null ? $sales->color : $sales->color_name;
            })
            ->addColumn('n_week',function($sales){
                return $sales->week_name == null ? $sales->week : $sales->week_name;
            })
            ->toJson();
    }

    // public function getList
}
