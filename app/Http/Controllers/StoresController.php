<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Store;
use DataTables;

class StoresController extends Controller
{
    public function index() {
        return view('stores.index');
    }
    
    public function datatable() {
        $stores = Store::join('users','users.id','stores.creator_id')
        // Store::join('dealers','users.id','stores.creator_id')
        ->select('users.name as creator_name','stores.*')->get();
        return DataTables::of($stores)
            // ->addColumn('action',function($stores){
            //     return view('includes.action-btn',[
            //         'label' => 'PREVIEW',
            //         'id' => '',
            //         'value' => route('store-management.show',[$stores->store_pid]),
            //         'type' => 'btn-default',
            //         'link' => true
            //     ])->render().
            //     view('includes.action-btn',[
            //         'label' => 'EDIT',
            //         'id' => '',
            //         'value' => route('store-management.edit',[$stores->store_pid]),
            //         'type' => 'btn-primary',
            //         'link' => true
            //     ])->render().
            //     view('includes.action-btn',[
            //         'label' => 'DELETE',
            //         'id' => '',
            //         'data_id' => $stores->store_pid,
            //         'value' => '',
            //         'type' => 'btn-danger delete_btn',
            //         'link' => false
            //     ])->render();
            // })
            // ->addColumn('recommend',function($stores){
            //     return [
            //         'label' => $stores->isRecommend_Item ? 'REMOVE FROM RECOMMENDED' : 'ADD TO RECOMMENDED',
            //         'data_id' => $stores->store_pid,
            //         'type' => 'btn-success approve_btn',
            //         'link' => false,
            //         'value' => $stores->isRecommend_Item == 1 ? 0 : 1 ,
            //         'type' => ($stores->isRecommend_Item == 1 ? 'btn-danger' : 'btn-success').' update_reco' ,
            //     ];
            // })
            ->toJson();
    }
}
