<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Response;
use Auth;

class UserController extends Controller
{
    public function list() {
    	if (Auth::user()->role_id != 1) {
			return Response::json([
                'success' => false,
                'message' => 'User can\'t create user',
            ], 202);
		}

    	$users = User::leftJoin('roles', 'roles.id', 'users.role_id')
            ->select('users.*','roles.type as role_name')->get();
        return $users;
    }

    // public function listCon

    public function logoutApi(Request $request)
    { 
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();
        return response()->json(null, 204);
    }

    public function deactivateUser(Request $request) {
        DB::beginTransaction();
        try {
            $user = User::find($request->user_id);
            if(! $user ) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Employee not found.'
                ],200);
            }
            $user->update([
                'active' => 0
            ]);
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'User successfully deactivated.'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
       


    }
}
