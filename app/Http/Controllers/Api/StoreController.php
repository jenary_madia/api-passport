<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\AddStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Store;
use App\StoreVisibility;
use Validator;
use DB;
use Storage;
use Auth;
use testStoreRequest;

class StoreController extends Controller
{
    public function __construct() {
    	$this->middleware('auth');
    }

    public function add(AddStoreRequest $request) {

        DB::beginTransaction();
        try {
			$file_name = $request->file('image')->getClientOriginalName();
    		$store = Store::create([
				"name" => $request->name,
                "dealer_name" => $request->dealer_name,
				"creator_id" => Auth::user()->id,
				"mall_name" => $request->mall_name,
                "promoter" => $request->promoter,
                "brand_name" => $request->brand_name,
				"region" => $request->region,
				"image" => $file_name,
				"branch_address" => $request->branch_address,
				"branch_contact_no" => $request->branch_contact_no,
				"branch_email" => $request->branch_email,
				"weekly_sales_amount" => $request->weekly_sales_amount,
				"weekly_sales_qty" => $request->weekly_sales_qty,
				"existing_vd" => $request->existing_vd,
				"recent_dut" => $request->recent_dut,
				"recent_dum" => $request->recent_dum ? $request->recent_dum : "",
				"product_mix" => $request->product_mix,
			]);
            $request->file('image')->storeAs(
                "public/store-photo", $file_name
            );
            $visibilities = [];
            foreach ($request->visibilities as $v) {
            	array_push($visibilities,[
					"store_id" => $store->id,
					"description" => $v
            	]);
            }

            StoreVisibility::insert($visibilities);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Store successfully added'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function get($id) {
        return Store::with('visibilities')->join('users','stores.creator_id','users.id')->select('stores.*','users.name as creator_name')->where('stores.id',$id)->first();
        // return Store::with('visibilities')->where('id',$id)->first();
    }

    public function delete(Request $request) {
        DB::beginTransaction();
        try {
            $store = Store::find($request->store_id);
            if(! $store ) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Store not found.'
                ],200);
            }
            $store->update([
                'active' => 0
            ]);
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Store successfully deactivated.'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function update(UpdateStoreRequest $request,$id) {
        DB::beginTransaction();
        try {
            $store = Store::find($id);
            if(! $store) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid store'
                ],500);
            } 
            $params = [
                "name" => $request->name,
                "dealer_name" => $request->dealer_name,
                "mall_name" => $request->mall_name,
                "promoter" => $request->promoter,
                "brand_name" => $request->brand_name,
                "region" => $request->region,
                "branch_address" => $request->branch_address,
                "branch_contact_no" => $request->branch_contact_no,
                "branch_email" => $request->branch_email,
                "weekly_sales_amount" => $request->weekly_sales_amount,
                "weekly_sales_qty" => $request->weekly_sales_qty,
                "existing_vd" => $request->existing_vd,
                "recent_dut" => $request->recent_dut,
                "recent_dum" => $request->recent_dum ? $request->recent_dum : "",
                "product_mix" => $request->product_mix,
            ];

            if ($request->hasFile('image')) {
                $file_name = $request->file('image')->getClientOriginalName();
                $params['image'] = $file_name;
            }

            $store->update($params);
            if ($request->hasFile('image')) {
                $request->file('image')->storeAs(
                    "public/store-photo", $file_name
                );
            }

            $visibilities = [];
            if($request->visibilities) {
                foreach ($request->visibilities as $v) {
                    array_push($visibilities,[
                        "store_id" => $store->id,
                        "description" => $v
                    ]);
                }
                StoreVisibility::where("store_id",$id)->delete();
                StoreVisibility::insert($visibilities);
            }

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Store successfully updated'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function storeDetails(Request $request,$store_id) {
        DB::beginTransaction();
        try {
            StoreDetail::firstOrCreate([
                    'store_id' => $store_id
                ],
                [
                    'dealer_menu' => $request->get('dealer_menu'),
                    'region' => $request->get('region'),
                    'model' => $request->get('model'),
                    'color' => $request->get('color'),
                    'qty' => $request->get('qty'),
                    'down_payment' => $request->get('down_payment'),
                    'week_no' => $request->get('week_no'),
                    'date' => $request->get('date'),
                ]
            );
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Store details successfully added'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function list2(Request $request , $max_id = null) {
    	$stores = Store::with('visibilities')
        ->join('users','stores.creator_id','users.id');
        if(isset($request->search)) {
            $search = $request->search;
            $stores->where(function($query) use ($search){
                // $query->where('pr_reference_no','LIKE',"%$search%");
                $query->orWhere('users.name','LIKE',"%$search%");
                $query->orWhere('dealer_name','LIKE',"%$search%");
                $query->orWhere('mall_name','LIKE',"%$search%");
                $query->orWhere('stores.name','LIKE',"%$search%");
                // $query->orWhere('creator_name','LIKE',"%$search%");
            });
        }
        $stores->select('stores.*','users.name as creator_name')
        ->where('stores.active',1)
        ->orderBy('stores.id', 'desc');
        if($max_id) {
            $stores->where('stores.id','<',$max_id);
        }
        
        return $stores->limit(5)->get();
    }

    public function list() { // check if not needed
        return Store::with('visibilities')
        ->join('users','stores.creator_id','users.id')
        ->select('stores.*','users.name as creator_name')
        ->orderBy('stores.id', 'desc')
        ->get();
    }

    public function search(Request $request) {
        $search = $request->search;
        $stores = Store::with('visibilities')
        ->join('users','stores.creator_id','users.id')
        ->where(function($query) use ($search){
            // $query->where('pr_reference_no','LIKE',"%$search%");
            $query->orWhere('users.name','LIKE',"%$search%");
            $query->orWhere('dealer_name','LIKE',"%$search%");
            $query->orWhere('mall_name','LIKE',"%$search%");
            $query->orWhere('stores.name','LIKE',"%$search%");
            // $query->orWhere('creator_name','LIKE',"%$search%");
        })
        ->select('stores.*','users.name as creator_name')
        ->orderBy('stores.id', 'desc');

        // if($max_id) {
        //     $stores->where('stores.id','<',$max_id);
        // }
        
        return $stores->limit(5)->get();
    }

    // FOR TESTING ONLY
    public function addTest(testStoreRequest $request) {
        DB::beginTransaction();
        try {
            $file_name = $request->file('image')->getClientOriginalName();
            $store = Store::create([
                "name" => $request->name,
                "dealer_name" => $request->dealer_name,
                "creator_id" => Auth::user()->id,
                "mall_name" => $request->mall_name,
                "promoter" => $request->promoter,
                "brand_name" => $request->brand_name,
                "region" => $request->region,
                "image" => $file_name,
                "branch_address" => $request->branch_address,
                "branch_contact_no" => $request->branch_contact_no,
                "branch_email" => $request->branch_email,
                "weekly_sales_amount" => $request->weekly_sales_amount,
                "weekly_sales_qty" => $request->weekly_sales_qty,
                "existing_vd" => $request->existing_vd,
                "recent_dut" => $request->recent_dut,
                "recent_dum" => $request->recent_dum ? $request->recent_dum : "",
                "product_mix" => $request->product_mix,
            ]);
            $request->file('image')->storeAs(
                "public/store-photo", $file_name
            );
            $visibilities = [];
            foreach ($request->visibilities as $v) {
                array_push($visibilities,[
                    "store_id" => $store->id,
                    "description" => $v
                ]);
            }

            StoreVisibility::insert($visibilities);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Store successfully added'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
