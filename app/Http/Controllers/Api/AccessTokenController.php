<?php
namespace App\Http\Controllers\API;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Response;
use \Laravel\Passport\Http\Controllers\AccessTokenController as ATC;

class AccessTokenController extends ATC
{
    public function issueToken(ServerRequestInterface $request)
    {
        try {
            //get username (default is :email)
            $username = $request->getParsedBody()['username'];

            //change to 'email' if you want
            $user = User::where([
                'email' => $username,
                'active' => 1
            ])->first();

            if(! $user)
                throw new OAuthServerException('The user credentials were incorrect.', 6, 'invalid_credentials', 401);

            //generate token
            $tokenResponse = parent::issueToken($request);

            //convert response to json string
            $content = $tokenResponse->getContent();

            //convert json to array
            $data = json_decode($content, true);

            if(isset($data["error"]))
                throw new OAuthServerException('The user credentials were incorrect.', 6, 'invalid_credentials', 401);

            //update last logged in
            $user->update([
                'last_login' => \Carbon\Carbon::now()
            ]);

            //add access token to user
            $user = collect($user);
            $user->put('access_token', $data['access_token']);

            return Response::json($user,200);
        }
        catch (ModelNotFoundException $e) { // email notfound
            //return error message
            return Response::json(["message" => "The user credentials were incorrect"], 401);
        }
        catch (OAuthServerException $e) { //password not correct..token not granted
            //return error message
            return Response::json(["message" => "The user credentials were incorrect"], 401);
        }
        catch (Exception $e) {
            ////return error message
            return Response::json(["message" => "Internal server error"], 500);
        }
    }
}