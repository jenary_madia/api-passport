<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Dealers;
use App\Models;
use App\Weeks;
use App\Regions;
use App\Colors;
use Illuminate\Support\Facades\Response;
use Auth;
use DB;
use App\Sales;
use App\Http\Controllers\Controller;

class SalesController extends Controller
{
    public function fetchSelections() {
       $dropdowns = [
            "DEALER" => Dealers::where('active',1)->orderBy('name','asc')->select("id","name")->get(),
            "MODEL" =>  Models::where('active',1)->select("id","name")->get(),
            "REGION" =>  Regions::where('active',1)->select("id","name")->get(),
            "COLOR" =>  Colors::where('active',1)->select("id","name")->get(),
            "WEEK" =>  Weeks::where('active',1)->select("id","name")->get(),
        ];
        return $dropdowns;
    }

    public function store(Request $request) {
        DB::beginTransaction();
        try {
            if($request->dealer_id == 'other') {
                $dealer_menu = Dealers::create([
                    'name' => $request->dealer_other
                ])->id;
            }else{
                $dealer_menu = $request->dealer_id;
            }
            Sales::create(
                [
                    'creator_id' => Auth::user()->id,
                    'dealer_menu' => $dealer_menu,
                    'region' => $request->get('region'),
                    'model' => $request->get('model'),
                    'color' => $request->get('color'),
                    'qty' => $request->get('qty'),
                    'down_payment' => $request->get('down_payment'),
                    'week' => $request->get('week_no'),
                    'date' => $request->get('date'),
                ]
            );
            DB::commit();
            return Response::json([
                'success' => true,
                'message' => 'Sales details successfully added'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function list(Request $request) {
    	$sales = Sales::join('users','sales.creator_id','users.id')
                ->leftjoin('dealers','sales.dealer_menu','dealers.id')
                ->join('regions','sales.region','regions.id')
                ->leftjoin('models','sales.model','models.id')
                ->leftjoin('colors','sales.color','colors.id')
                ->leftjoin('weeks','sales.week','weeks.id')
                ->select(DB::raw('DATE_FORMAT(sales.date, "%m-%d-%Y") as sales_date'),'sales.*','users.name as creator_name','dealers.name as dealer_name','regions.name as region_name','models.name as model_name','colors.name as color_name','weeks.name as week_name')
                ->orderBy('sales.id', 'desc');

	        if(isset($request->max_id)) {
	            $sales->where('sales.id','<',$request->max_id);
	        }

            if(isset($request->date)) {
                $sales->whereRaw("DATE_FORMAT(sales.date, '%m-%d-%Y') = '$request->date' ");
            }	        
	        return $sales->limit(5)->get();
    }

    public function search(Request $request) {
        $sales = Sales::join('users','sales.creator_id','users.id')
                ->leftjoin('dealers','sales.dealer_menu','dealers.id')
                ->join('regions','sales.region','regions.id')
                ->leftjoin('models','sales.model','models.id')
                ->leftjoin('colors','sales.color','colors.id')
                ->leftjoin('weeks','sales.week','weeks.id')
                ->select(DB::raw('DATE_FORMAT(sales.date, "%m-%d-%Y") as sales_date'),'sales.*','users.name as creator_name','dealers.name as dealer_name','regions.name as region_name','models.name as model_name','colors.name as color_name','weeks.name as week_name')
                ->orderBy('sales.id', 'desc');

            if($request->has('date')) {
                $sales->whereRaw("DATE_FORMAT(sales.date, '%m-%d-%Y') = '$request->date' ");
            }
            
            return $sales->limit(5)->get();
    }
}

