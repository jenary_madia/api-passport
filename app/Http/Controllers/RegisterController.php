<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Response;
use App\User;
use Auth;

class RegisterController extends Controller
{
    public function register(StoreUserRequest $request) {
    	if (Auth::user()->role_id != 1) {
			return Response::json([
                'success' => false,
                'message' => 'User can\'t create user',
            ], 202);
		}
    	return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id,
            'password' => bcrypt($request->password),
        ]);
    }
}
