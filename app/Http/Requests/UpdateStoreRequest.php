<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|unique:stores,id,".$this->get('id'),
            "dealer_name" => "required",
            "mall_name" => "required",
            "promoter" => "required",
            "brand_name" => "required",
            "mall_name" => "required",
            "region" => "required",
            "branch_address" => "required",
            "branch_contact_no" => "required",
            "branch_email" => "required",
            "weekly_sales_amount" => "required",
            "weekly_sales_qty" => "required",
            "existing_vd" => "required",
            "recent_dut" => "required",
            "recent_dum" => "required",
            "product_mix" => "required",
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function response(array $errors)
    {
        return Response::json($errors);
        // return Response::create([
        //     'success' => false,
        //     'message' => 'something went wrong',
        //     "errors" =>$errors,
        // ], 500);
    }

    protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(Response::json([
            "message" => "The given data is invalid.".json_encode($validator->errors()->all()),
            "errors" => $validator->errors()->all(),
        ],500)); 
    }


}
