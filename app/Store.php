<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $guarded = [];

    public function visibilities() {
    	return $this->hasMany('App\StoreVisibility','store_id','id');
    } 
}
