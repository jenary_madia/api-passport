<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreVisibility extends Model
{
    protected $table = "store_visibilities";
    protected $guarded = [];
}
